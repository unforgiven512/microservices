from app.api.models import *
from app.api import db_manager

from fastapi import APIRouter,HTTPException
from typing import List

casts = APIRouter()

@casts.get('/',response_model = List[CastOut],status_code =200)
async def get_cast():
    return await db_manager.get_all_casts()

@casts.post('/',response_model = CastOut,status_code =201)
async def create_cast(payload: CastIn):
    cast_id = await db_manager.add_casts(payload)
    response = {'id':cast_id,**payload.dict()}
    return response

@casts.put('/{id}',response_model = CastOut,status_code =200)
async def update_cast(id:int,payload:CastUpdate):
    cast = await db_manager.get_cast_by_id(id)
    if not cast:
        raise HTTPException(status_code=404, detail="Cast not found")
    update_data = payload.dict(exclude_unset = True)
    cast_in_db = CastIn(**cast)
    cast_udpated = cast_in_db.copy(update = update_data)
    return await db_manager.update_cast(id,cast_udpated)

@casts.get('/{id}/',response_model = CastOut,status_code =200)
async def get_cast_detail(id:int):
    cast = await db_manager.get_cast_by_id(id)
    if not cast:
        raise HTTPException(status_code=404,detail='Cast not found')
    return cast

@casts.delete('/{id}/',response_model = None)
async def delete_cast(id:int):
    cast = await db_manager.get_cast_by_id(id)
    if not cast:
        raise HTTPException(status_code=404,detail='Cast not found')
    return await db_manager.delete_cast(id)



