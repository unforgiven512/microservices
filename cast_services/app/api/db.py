import os
from databases import Database
from sqlalchemy import (Column,Table,MetaData,String,Integer,create_engine,ARRAY)

DATABASE_URL = os.getenv('DATABASE_URI')
# DATABASE_URL = 'postgresql://postgres:postgres@localhost/cast_db'
engine = create_engine(DATABASE_URL)
metadata = MetaData()

casts = Table(
    'casts',
    metadata,
    Column('id',Integer,primary_key=True),
    Column('name',String(50))
)

database = Database(DATABASE_URL)